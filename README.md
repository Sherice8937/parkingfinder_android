# README #

Parking Finder
by Sherice Sutcliffe

### About ###

* Parking Finder was created from July 31st - September 23rd, 2017 (2 months approx.)
* It displays available parking and information about each spot, to assist users in finding good parking.

### Set Up Requirements ###

* Up-To-Date Google Play Services
* Internet/WiFi
* 21+ API
* Android Pay (reccommended, NOT required)

### Contact ###

* Repo owner/admin: Sherice Sutcliffe
* Email: sherice.sutcliffe8937@yahoo.com

## Please contact me if you wish to download/use this project in any way