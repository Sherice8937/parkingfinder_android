package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sherice on 9/3/17.
 * Parking Finder
 */

public class HistoryBaseAdapter extends BaseAdapter {

    private static final int baseID = 0x0A0B0CD;

    // reference to owning screen
    private final Context context;
    // reference to collection
    private final ArrayList<Reservation> allReservedSpots;

    // default constructor
    HistoryBaseAdapter(Context _context, ArrayList<Reservation> allReservedSpots) {
        context = _context;
        this.allReservedSpots = allReservedSpots;
    }

    public int getCount() {
        if (allReservedSpots != null) {
            return allReservedSpots.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        if (allReservedSpots != null && position < allReservedSpots.size() && position > -1) {
            return allReservedSpots.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return baseID + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        HistoryBaseAdapter.ViewHolder viewHolder;

        // check for a recycled view first
        if (convertView == null) {
            // if there isn't a recycled view, create one using inflater
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.history_list_layout, parent, false);
            viewHolder = new HistoryBaseAdapter.ViewHolder((convertView));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (HistoryBaseAdapter.ViewHolder)convertView.getTag();
        }

        // get the object for this position
        Reservation reservation = (Reservation) getItem(position);

        String price = "$" + reservation.getPrice();

        // setup the view using ViewHolder
        viewHolder.time.setText(reservation.getTime());
        viewHolder.price.setText(price);
        viewHolder.title.setText(reservation.getTitle());
        viewHolder.date.setText(reservation.getDate());

        // return the view
        return convertView;
    }

    static class ViewHolder {

        // Views from layout
        public final TextView title;
        public final TextView time;
        public final TextView date;
        public final TextView price;

        // constructor that sets the views up
        public ViewHolder(View v) {
            title = (TextView)v.findViewById(R.id.locationNameText_History);
            time = (TextView)v.findViewById(R.id.durationText_History);
            date = (TextView)v.findViewById(R.id.dateText_History);
            price = (TextView)v.findViewById(R.id.priceText_History);
        }
    }
}
