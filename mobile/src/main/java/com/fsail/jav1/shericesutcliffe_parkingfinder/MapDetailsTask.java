package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sherice on 8/21/17.
 * Parking Finder
 */

class MapDetailsTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "MapDetailsTask";

    private final String mApiUrl;
    private final Context mContext;
    private final TextView mSpots;
    private final TextView mDescription;
    private final TextView mDirections;

    private String mSpotsText;
    private String mDescriptionText;
    private String mDirectionsText;


    public MapDetailsTask(Context context, String apiUrl, TextView spots, TextView description, TextView directions) {
        mContext = context;
        mSpots = spots;
        mDescription = description;
        mDirections = directions;
        mApiUrl = apiUrl;
    }

    @Override
    protected Void doInBackground(Void... params) {
        // connect to the ParkWhiz URL
        try {
            // Create URL
            URL parkWhizURL = new URL(mApiUrl + "&" + ParkingFinderUtils.KEY);

            // Create connection
            HttpsURLConnection myConnection =
                    (HttpsURLConnection) parkWhizURL.openConnection();
            // set request property
            myConnection.setRequestProperty("User-Agent",
                    "fec4fd64d0fc97d65fa170c55e984caaa5e56111-sherice-sutcliffe-request");

            // check for valid response
            if (myConnection.getResponseCode() == 200) {
                // Success
                // get a reference to the input stream of the connection
                InputStream response = myConnection.getInputStream();
                String data = IOUtils.toString(response);

                // parse JSON
                parseJSONData(data);

            } else {
                Log.i(TAG, "doInBackground: AN ERROR HAS OCCURRED");
                Log.i(TAG, "doInBackground: RESPONSE CODE: " + myConnection.getResponseCode());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void parseJSONData(String data) {
        try {
            JSONObject outerMostObj = new JSONObject(data);
            String description = outerMostObj.getString("description");
            String directions = outerMostObj.getString("directions");
            JSONArray parkingListings = outerMostObj.getJSONArray("listings");
            // loop through each parking space returned
            for (int i = 0; i < parkingListings.length(); i++) {
                JSONObject parkingSpace = parkingListings.getJSONObject(i);
                String spots = parkingSpace.getString("available_spots");

                // update member variables
                mDescriptionText = description;
                mDirectionsText = directions;
                mSpotsText = spots;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        // update UI
        mSpots.setText(mSpotsText);
        mDescription.setText(mDescriptionText);
        mDirections.setText(mDirectionsText);
    }
}
