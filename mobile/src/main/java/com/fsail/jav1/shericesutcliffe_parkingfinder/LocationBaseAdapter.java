package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


class LocationBaseAdapter extends android.widget.BaseAdapter {

    private static final int baseID = 0x0A0B0CD;

    // reference to owning screen
    private final Context context;
    // reference to collection
    private final ArrayList<ParkingLoc> allParkingLocs;

    // default constructor
    LocationBaseAdapter(Context _context, ArrayList<ParkingLoc> _allParkingLocs) {
        context = _context;
        allParkingLocs = _allParkingLocs;
    }

    public int getCount() {
        if (allParkingLocs != null) {
            return allParkingLocs.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        if (allParkingLocs != null && position < allParkingLocs.size() && position > -1) {
            return allParkingLocs.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return baseID + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        // check for a recycled view first
        if (convertView == null) {
            // if there isn't a recycled view, create one using inflater
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.listview_layout, parent, false);
            viewHolder = new ViewHolder((convertView));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        // get the object for this position
        ParkingLoc parkingLoc = (ParkingLoc) getItem(position);

        // setup the view using ViewHolder
        viewHolder.locationName.setText(parkingLoc.getLocationName());
        viewHolder.price.setText(parkingLoc.getPriceFormatted());

        // concatenate full address (address, city, state, and zip)
        String fullAddress = parkingLoc.getAddress() + ", " + parkingLoc.getCity() +
                ", " + parkingLoc.getState() + " " + parkingLoc.getZip();

        viewHolder.address.setText(fullAddress);

        // convert distance in feet to miles
        int distanceInt = parkingLoc.getDistance();

        String distance;
        if (distanceInt <= 1) {
            distance = "<1 mile away";
        } else {
            distance = String.valueOf(distanceInt) + " miles away";
        }

        viewHolder.distance.setText(distance);

        // return the view
        return convertView;
    }

    static class ViewHolder {

        // Views from layout
        public final TextView locationName;
        public final TextView address;
        public final TextView distance;
        public final TextView price;

        // constructor that sets the views up
        public ViewHolder(View v) {
            locationName = (TextView)v.findViewById(R.id.locationNameText);
            address = (TextView)v.findViewById(R.id.addressText);
            distance = (TextView)v.findViewById(R.id.distanceText);
            price = (TextView)v.findViewById(R.id.priceText);
        }
    }
}
