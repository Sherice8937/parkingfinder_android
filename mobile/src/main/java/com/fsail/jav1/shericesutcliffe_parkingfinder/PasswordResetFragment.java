package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;


/**
 * Created by Sherice on 8/23/17.
 * Parking Finder
 */

public class PasswordResetFragment extends Fragment implements View.OnClickListener {

    private EditText email;

    public static PasswordResetFragment newInstance() {

        Bundle args = new Bundle();

        PasswordResetFragment fragment = new PasswordResetFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.reset_password_fragment, container, false);

        email = (EditText) inflateView.findViewById(R.id.emailText);
        Button submit = (Button)inflateView.findViewById(R.id.resetPasswordButton);
        submit.setOnClickListener(this);

        return inflateView;
    }

    @Override
    public void onClick(View v) {
        final String emailText = email.getText().toString().trim();

        // make sure field is not empty
        if (TextUtils.isEmpty(emailText)) {
            Toast.makeText(getContext(), "Enter your registered email id", Toast.LENGTH_SHORT).show();
            return;
        }

        FirebaseAuth auth = FirebaseAuth.getInstance();

        auth.sendPasswordResetEmail(emailText)
                .addOnCompleteListener(new OnCompleteListener<Void>() {

                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                        if (task.isSuccessful()) {
                            // do something when mail was sent successfully.
                            Toast.makeText(getContext(), "Email sent!", Toast.LENGTH_LONG).show();
                            email.setText("");

                        } else {
                            Toast.makeText(getContext(), "There was an error, please try again!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
