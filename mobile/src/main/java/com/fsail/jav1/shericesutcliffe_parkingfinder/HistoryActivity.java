package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class HistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        // get access to toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // update UI
        toolbar.setTitle("Your Reservation History");
        setSupportActionBar(toolbar);

        setUpToolbar(toolbar);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.historyFrameLayout, HistoryFragment.newInstance()).commit();
        }
    }

    private void setUpToolbar(Toolbar toolbar) {

        // set up back button on Toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // set up navigation
        // so user can go back
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                finish();
            }
        });
    }
}
