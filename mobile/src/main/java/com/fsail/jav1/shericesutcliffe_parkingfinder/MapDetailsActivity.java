package com.fsail.jav1.shericesutcliffe_parkingfinder;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.wallet.WalletConstants;

import static com.fsail.jav1.shericesutcliffe_parkingfinder.MapDetailsFragment.MASKED_WALLET_REQUEST_CODE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_CURRENT_LAT;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_CURRENT_LONG;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_DEST_LAT;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_DEST_LONG;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.GET_DIRECTIONS;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.MAP_DETAILS_FRAGMENT_ID;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.MAP_FRAGMENT_ID;

public class MapDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_details);
        // get access to toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // get title passed in from intent
        Intent dataIntent = getIntent();
        String title = dataIntent.getStringExtra(ParkingFinderUtils.EXTRA_TITLE);
        // update UI
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        // update toolbar UI
        setUpToolbar(toolbar);

        // get access to FAB
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.details_fab_directions);
        // update FAB UI
        setUpFAB(fab);

        if (savedInstanceState == null) {
            // inflate fragment
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mapDetailsFrameLayout, MapDetailsFragment.newInstance()).commit();
        }
    }

    private void setUpToolbar(Toolbar toolbar) {

        // set up back button on Toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // set up navigation
        // so user can go back
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                finish();
            }
        });
    }

    private void setUpFAB(FloatingActionButton fab) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "LOADING DIRECTIONS...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                // set to false until data is checked
                GET_DIRECTIONS = false;

                // get data that is passed in
                Intent data = getIntent();
                Double currentLat = data.getDoubleExtra(EXTRA_CURRENT_LAT, 0);
                Double currentLong = data.getDoubleExtra(EXTRA_CURRENT_LONG, 0);
                Double destinationLat = data.getDoubleExtra(EXTRA_DEST_LAT, 0);
                Double destinationLong = data.getDoubleExtra(EXTRA_DEST_LONG, 0);

                // check that the correct values are passed in
                if (currentLat != 0 && currentLong != 0 && destinationLat != 0
                        && destinationLong != 0) {
                    // if they are, update GET_DIRECTIONS so map knows to get directions
                    GET_DIRECTIONS = true;
                    // then go back to the MapActivity
                    finish();

                } else {
                    Snackbar.make(view, "Error getting directions, please try again", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // handle onActivityResult in MapDetailsFragment
        MapDetailsFragment mapDetailsFragment = (MapDetailsFragment) getSupportFragmentManager().findFragmentById(MAP_DETAILS_FRAGMENT_ID);
        mapDetailsFragment.onActivityResult(requestCode, resultCode, data);
    }
}
