package com.fsail.jav1.shericesutcliffe_parkingfinder;

/**
 * Created by Sherice on 9/2/17.
 * Parking Finder
 */

public class Reservation {

    String title;
    String price;
    String time;
    String date;

    public Reservation(String title, String price, String time, String date) {
        this.title = title;
        this.price = price;
        this.time = time;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getTime() {
        return time;
    }

    public String getDate() {
        return date;
    }
}
