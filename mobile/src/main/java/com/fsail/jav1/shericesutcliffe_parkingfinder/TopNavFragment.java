package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Sherice on 9/20/17.
 */

public class TopNavFragment extends Fragment {

    public static TopNavFragment newInstance() {

        Bundle args = new Bundle();

        TopNavFragment fragment = new TopNavFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.top_nav_layout, container, false);

        return inflateView;
    }
}
