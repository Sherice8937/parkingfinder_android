package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.ArrayList;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.ACTION_UPDATE_LIST;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.CURRENT_LATITUDE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.CURRENT_LONGITUDE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_ADDRESS;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_PRICE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_TITLE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_URL;

/**
 * Created by Sherice on 8/20/17.
 * Parking Finder
 */

public class ListFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ArrayList<ParkingLoc> mAllParkingSpaces = new ArrayList<>();
    private LocationBaseAdapter mBaseAdapter;
    private ListView mListView;
    ParkingSpotsInterface mInterface;
    private ListFilterReceiver mReceiver;

    public static ListFragment newInstance() {

        Bundle args = new Bundle();

        ListFragment fragment = new ListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

        // create mReceiver
        mReceiver = new ListFilterReceiver();

        // listen for update intent
        IntentFilter update = new IntentFilter(ACTION_UPDATE_LIST);

        // register the mReceiver
        getActivity().registerReceiver(mReceiver, update);

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ParkingFinderUtils.LIST_FRAGMENT_ID = ListFragment.super.getId();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setHasOptionsMenu(true);
        if (context instanceof ParkingSpotsInterface) {
            mInterface = (ParkingSpotsInterface) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView =inflater.inflate(R.layout.list_fragment, container, false);

        // get allParkingSpots from Map Activity
        mAllParkingSpaces = mInterface.getAllParkingSpaces();

        mListView = (ListView)inflateView.findViewById(R.id.mapListView);
        mBaseAdapter = new LocationBaseAdapter(getContext(), mAllParkingSpaces);
        mListView.setAdapter(mBaseAdapter);
        mListView.setOnItemClickListener(this);
        mBaseAdapter.notifyDataSetChanged();

        return inflateView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check that the result was from the autocomplete widget.
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(getContext(), data);

                // get the latitude and longitude from the Intent
                // update user's current location
                CURRENT_LATITUDE = place.getLatLng().latitude;
                CURRENT_LONGITUDE = place.getLatLng().longitude;

                // now update the list to display user's new current location
                mInterface.startTask(null, CURRENT_LATITUDE, CURRENT_LONGITUDE, mBaseAdapter, null);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Toast.makeText(getContext(), "AN ERROR HAS OCCURRED, PLEASE TRY AGAIN", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                // if user closes before selecting a place...
                // do nothing
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // get details of the item selected
        String apiUrl = mAllParkingSpaces.get(position).getApiUrl();
        String address = mAllParkingSpaces.get(position).getAddress();
        String price = mAllParkingSpaces.get(position).getPriceFormatted();
        String title = mAllParkingSpaces.get(position).getLocationName();

        // send data to MapDetails
        Intent toDetails = new Intent(getContext(), MapDetailsActivity.class);
        toDetails.putExtra(EXTRA_URL, apiUrl);
        toDetails.putExtra(EXTRA_ADDRESS, address);
        toDetails.putExtra(EXTRA_PRICE, price);
        toDetails.putExtra(EXTRA_TITLE, title);
        startActivity(toDetails);
    }

    private void updateList() {
        // first empty the list
        // to avoid repeats

        // get the new filtered list
        mAllParkingSpaces = mInterface.getAllParkingSpaces();
        // update list
        mBaseAdapter = new LocationBaseAdapter(getContext(), mAllParkingSpaces);
        mListView.setAdapter(mBaseAdapter);
        mBaseAdapter.notifyDataSetChanged();
    }

    class ListFilterReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // update the map
            updateList();
        }
    }
}
