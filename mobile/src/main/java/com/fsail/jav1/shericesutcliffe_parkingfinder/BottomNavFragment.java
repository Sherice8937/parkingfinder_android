package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Sherice on 9/20/17.
 */

public class BottomNavFragment extends Fragment {

    public static BottomNavFragment newInstance() {

        Bundle args = new Bundle();

        BottomNavFragment fragment = new BottomNavFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.bottom_nav_layout, container, false);

        return inflateView;
    }
}
