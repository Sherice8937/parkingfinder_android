package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.Constants;
import com.mapbox.services.android.location.LostLocationEngine;
import com.mapbox.services.android.navigation.v5.MapboxNavigation;
import com.mapbox.services.android.navigation.v5.listeners.NavigationEventListener;
import com.mapbox.services.android.navigation.v5.routeprogress.ProgressChangeListener;
import com.mapbox.services.android.navigation.v5.routeprogress.RouteProgress;
import com.mapbox.services.android.telemetry.location.LocationEngine;
import com.mapbox.services.api.ServicesException;
import com.mapbox.services.api.directions.v5.DirectionsCriteria;
import com.mapbox.services.api.directions.v5.MapboxDirections;
import com.mapbox.services.api.directions.v5.models.DirectionsResponse;
import com.mapbox.services.api.directions.v5.models.DirectionsRoute;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.ACTION_UPDATE_MAP;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.CURRENT_LATITUDE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.CURRENT_LONGITUDE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.CURRENT_STEP;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_ADDRESS;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_CURRENT_LAT;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_CURRENT_LONG;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_DEST_LAT;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_DEST_LONG;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_PRICE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_TITLE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_URL;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.GET_DIRECTIONS;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.LIST_FILTERED;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.MAX_DISTANCE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.NOTIFICATION_MESSAGE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.TOTAL_STEPS;

/**
 * Created by Sherice on 9/3/17.
 * Parking Finder
 */

public class MapFragment extends Fragment implements LocationListener, View.OnClickListener {

    private static final String TAG = "MapFragment";

    public MapView mMapView;
    private LocationManager mLocationManager;

    // array to hold all parking spaces
    ArrayList<ParkingLoc> mAllParkingSpaces = new ArrayList<>();

    private Double mDestLat;
    private Double mDestLong;

    private ParkingSpotsInterface mInterface;
    private MapFilterReceiver mReceiver;

    MapboxNavigation mNavigation;

    private Messenger mService;
    private boolean mBound;

    private DirectionsRoute mRoute;
    private Response<DirectionsResponse> mResponse;

    public static MapFragment newInstance() {

        Bundle args = new Bundle();

        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mService = new Messenger(service);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getContext(), "pk.eyJ1Ijoic2hlcmljZTg5MzciLCJhIjoiY2o1eWlnbjJ6MDA3bTMybzA1cTljaTV4biJ9.a6i7UoOzBcTlZBN_GqvkMQ");

        ParkingFinderUtils.MAP_FRAGMENT_ID = MapFragment.super.getId();

         mNavigation = new MapboxNavigation
                (getContext(), "pk.eyJ1Ijoic2hlcmljZTg5MzciLCJhIjoiY2o1eWlnbjJ6MDA3bTMybzA1cTljaTV4biJ9.a6i7UoOzBcTlZBN_GqvkMQ");

        LocationEngine locationEngine = LostLocationEngine.getLocationEngine(getContext());
        mNavigation.setLocationEngine(locationEngine);
        mNavigation.addProgressChangeListener(new ProgressChangeListener() {
            @Override
            public void onProgressChange(Location location, RouteProgress routeProgress) {
                // update UI & send notification to Android Auto
                mRoute = routeProgress.getRoute();
                userProgressChanged();
            }
        });
    }

    private void userProgressChanged() {
        TOTAL_STEPS = mRoute.getLegs().get(0).getSteps().size();
        if (CURRENT_STEP == TOTAL_STEPS-1) {
            // this means the user has reached their destination
            // do nothing

        } else {
            CURRENT_STEP += 1;
            // formatter to round double to second decimal place
            DecimalFormat formatter = new DecimalFormat("###.##");
            DecimalFormat timeFormatter = new DecimalFormat("###");

            // get distance and time
            Double distanceDouble = mRoute.getDistance();
            Double timeDouble = mRoute.getDuration();

            // convert to miles & seconds
            timeDouble = timeDouble / 60;
            String time = String.valueOf(timeFormatter.format(timeDouble) + " min");

            distanceDouble = distanceDouble / 1609.34;
            String distance = String.valueOf(formatter.format(distanceDouble) + " miles");

            // calculate arrival time
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            // get the current time in seconds
            int currentTime = cal.get(Calendar.MINUTE);
            // add timeDouble to current time
            currentTime = currentTime + timeDouble.intValue();
            String arrivalTime = String.valueOf(Calendar.HOUR + ":" + timeFormatter.format(currentTime));

            // get destination name
            String wayPoint = mResponse.body().getWaypoints().get(1).getName();

            // get distance till next turn
            Double distanceTempDouble = mRoute.getLegs().get(0).getSteps().get(CURRENT_STEP).getDistance();
            distanceTempDouble = distanceTempDouble / 1609.34;
            String distanceTemp = String.valueOf(formatter.format(distanceTempDouble) + " miles");

            // get name of street user is on
            String currentStreet = String.valueOf(mRoute.getLegs().get(0).getSteps().get(CURRENT_STEP).getName());

            // get current instruction
            String instruction = mRoute.getLegs().get(0).getSteps().get(CURRENT_STEP).getManeuver().getInstruction();
            NOTIFICATION_MESSAGE = instruction;

            double[] sim_location = mRoute.getLegs().get(0).getSteps().get(CURRENT_STEP).getManeuver().getLocation();

            // update UI
            mInterface.updateDirectionsUI(distance, time, wayPoint, distanceTemp, currentStreet, instruction, arrivalTime);

            // send notification to Android Auto
            sendNotification(1, CURRENT_STEP);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ParkingSpotsInterface) {
            mInterface = (ParkingSpotsInterface)context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.map_fragment, container, false);

        // set up MapBox MapView
        mMapView = (MapView) inflateView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.setStyleUrl("mapbox://styles/sherice8937/cj5yip8l9056v2so9idutlyxc");

        // check if we already have current location
        if (CURRENT_LATITUDE != null && CURRENT_LONGITUDE != null) {
            // if we do, just set up the map
            setUpMap();
        } else {
            // if not, get current location
            checkPermissions();
        }

        return inflateView;
    }

    private void requestPermissions() {
        // request permissions
        ActivityCompat.requestPermissions(getActivity(),
                new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
    }

    private boolean checkPermissions() {
        // get location manager
        mLocationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);

        // check for permissions
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // if we do, get last known location & check if it's a valid location
            Location lastKnown = mLocationManager.getLastKnownLocation
                    (LocationManager.NETWORK_PROVIDER);

            if (lastKnown != null) {
                CURRENT_LATITUDE = lastKnown.getLatitude();
                CURRENT_LONGITUDE = lastKnown.getLongitude();
                mLocationManager.removeUpdates(this);
                // set up the map using the user's current location
                setUpMap();
            } else {
                Toast.makeText(getContext(), "Current Location not found. Try Searching for your location",
                        Toast.LENGTH_LONG).show();
            }

            return true;

        }

        requestPermissions();

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // check if the user has allowed the app the use their current location
        if(!checkPermissions()) {
            Toast.makeText(getContext(), "We need your location to use the app!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.toolbar) {
            // back button pressed
            // clear the map and reload map markers
            mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    // make SearchView visible and toolbar invisible
                    mInterface.directionsStopped();

                    updateMap();
                }
            });
        }
    }

    // Called after openAutocompleteActivity() has finished, to return its result.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check that the result was from the autocomplete widget.
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(getContext(), data);

                // get the latitude and longitude from the Intent
                // update user's current location
                CURRENT_LATITUDE = place.getLatLng().latitude;
                CURRENT_LONGITUDE = place.getLatLng().longitude;

                // now update the map to display the user's new current location
                setUpMap();

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Toast.makeText(getContext(), "AN ERROR HAS OCCURRED, PLEASE TRY AGAIN", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                // if user closes before selecting a place...
                // do nothing
            }
        } else if (resultCode == 0) {
            // user has returned from Details Activity

            // check if they are getting directions or not
            if (GET_DIRECTIONS) {
                // if they are, get directions & update the toolbar
                getDirections();
                mInterface.directionsStarted();
            } // if not, do nothing
        }
    }

    public void setUpMap() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                // set up icon for current location marker
                IconFactory iconFactory = IconFactory.getInstance(getContext());
                Icon icon = iconFactory.fromResource(R.drawable.mapbox_mylocation_icon_default);

                MarkerViewOptions markerViewOptions = new MarkerViewOptions()
                        .position(new LatLng(CURRENT_LATITUDE, CURRENT_LONGITUDE))
                        .icon(icon);
                mapboxMap.addMarker(markerViewOptions);
                // set on click listener
                mapboxMap.setOnInfoWindowClickListener(new MapboxMap.OnInfoWindowClickListener() {
                    @Override
                    public boolean onInfoWindowClick(@NonNull Marker marker) {
                        infoWindowClicked(marker);
                        return true;
                    }
                });
                zoomInCamera(mapboxMap);
                // start task to update MapView
                mInterface.startTask(mMapView, CURRENT_LATITUDE, CURRENT_LONGITUDE, null, mInterface);
            }
        });
    }

    private void updateMap() {
        // get allParkingSpaces
        mAllParkingSpaces = mInterface.getAllParkingSpaces();

        // update the Map
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                // first clear the map
                // to avoid repeats
                mapboxMap.clear();
                // set up icon & marker for current location marker
                IconFactory iconFactory = IconFactory.getInstance(getContext());
                Icon icon = iconFactory.fromResource(R.drawable.mapbox_mylocation_icon_default);

                MarkerViewOptions currentMarkerViewOptions = new MarkerViewOptions()
                        .position(new LatLng(CURRENT_LATITUDE, CURRENT_LONGITUDE))
                        .icon(icon);
                mapboxMap.addMarker(currentMarkerViewOptions);

                for (int i = 0; i < mAllParkingSpaces.size(); i++) {
                    MarkerViewOptions markerViewOptions = new MarkerViewOptions()
                            .position(new LatLng(mAllParkingSpaces.get(i).getLatitude(),
                                    mAllParkingSpaces.get(i).getLongitude()))
                            .title(mAllParkingSpaces.get(i).getLocationName());
                    if (mAllParkingSpaces.get(i).getDistance() <= 1) {
                        markerViewOptions.snippet("<1 mile away" +
                                " | " + mAllParkingSpaces.get(i).getPriceFormatted());
                    } else {
                        markerViewOptions.snippet((mAllParkingSpaces.get(i).getDistance() + " miles away" +
                                " | " + mAllParkingSpaces.get(i).getPriceFormatted()));
                    }
                    mapboxMap.addMarker(markerViewOptions);
                }
            }
        });
    }

    private void zoomInCamera(MapboxMap mapboxMap) {
        if (mapboxMap == null || CURRENT_LATITUDE == null || CURRENT_LONGITUDE == null) {
            Toast.makeText(getContext(), "Location Not Found", Toast.LENGTH_LONG).show();
            return;
        }

        // update camera position so it is not zoomed in so far
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(CURRENT_LATITUDE, CURRENT_LONGITUDE)) // Sets the new camera position
                .zoom(14) // Sets the zoom to level 14
                .build(); // Builds the CameraPosition object from the builder
        mapboxMap.setCameraPosition(position);

        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
    }

    private void infoWindowClicked(Marker marker) {
        // get LatLng of map marker
        LatLng position = marker.getPosition();

        // get parking spots from MapActivity
        mAllParkingSpaces = mInterface.getAllParkingSpaces();

        // find out which item was clicked, based on position
        for (int i = 0; i < mAllParkingSpaces.size(); i++) {
            if (mAllParkingSpaces.get(i).getLatitude() == position.getLatitude()
                    && mAllParkingSpaces.get(i).getLongitude() == position.getLongitude()) {
                // once item is found (based on position),
                // pass these contents to Map Details
                Intent toDetails = new Intent(getContext(), MapDetailsActivity.class);
                toDetails.putExtra(EXTRA_TITLE, mAllParkingSpaces.get(i).getLocationName());
                toDetails.putExtra(EXTRA_URL, mAllParkingSpaces.get(i).getApiUrl());
                toDetails.putExtra(EXTRA_ADDRESS, mAllParkingSpaces.get(i).getAddress());
                toDetails.putExtra(EXTRA_PRICE, mAllParkingSpaces.get(i).getPriceFormatted());
                toDetails.putExtra(EXTRA_CURRENT_LAT, CURRENT_LATITUDE);
                toDetails.putExtra(EXTRA_CURRENT_LONG, CURRENT_LONGITUDE);
                toDetails.putExtra(EXTRA_DEST_LAT, mAllParkingSpaces.get(i).getLatitude());
                toDetails.putExtra(EXTRA_DEST_LONG, mAllParkingSpaces.get(i).getLongitude());

                // update destination long/lat
                // in case user gets directions
                mDestLat = mAllParkingSpaces.get(i).getLatitude();
                mDestLong = mAllParkingSpaces.get(i).getLongitude();

                // now go to Map Details
                startActivityForResult(toDetails, 0);
            }
        }
    }

    private void getDirections() {
        // update the Origin and Destination
        final Position origin = Position.fromCoordinates(CURRENT_LONGITUDE, CURRENT_LATITUDE);
        final Position destination = Position.fromCoordinates(mDestLong, mDestLat);

        // first clear the map
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.clear();

                // set up icon for current location marker
                IconFactory iconFactory = IconFactory.getInstance(getContext());
                Icon icon = iconFactory.fromResource(R.drawable.mapbox_mylocation_icon_default);

                // now add the origin and destination to the map
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(origin.getLatitude(), origin.getLongitude()))
                .icon(icon));
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(destination.getLatitude(), destination.getLongitude())));
            }
        });

        // lastly, get the route from API
        try {
            getRoute(origin, destination);
        } catch (ServicesException e) {
            e.printStackTrace();
        }
    }

    private void getRoute(Position origin, Position destination) throws ServicesException {

        MapboxDirections client = new MapboxDirections.Builder()
                .setOrigin(origin)
                .setDestination(destination)
                .setProfile(DirectionsCriteria.PROFILE_DRIVING)
                .setAccessToken("pk.eyJ1Ijoic2hlcmljZTg5MzciLCJhIjoiY2o1eWlnbjJ6MDA3bTMybzA1cTljaTV4biJ9.a6i7UoOzBcTlZBN_GqvkMQ")
                .setSteps(true)
                .build();

        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call,
                                   Response<DirectionsResponse> response) {

                mResponse = response;

                // if there was ane error getting the response
                if (response.body() == null) {
                    Toast.makeText(getContext(),
                            "No routes found, please try again.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                // get current route info
                mRoute = response.body().getRoutes().get(0);

                // draw the route on the map
                drawRoute();
                updateDirectionsUI();
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                Toast.makeText(getContext(), "Error: " + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateDirectionsUI() {
        userProgressChanged();
    }

    private void drawRoute() {
        // Convert LineString coordinates into LatLng[]
        LineString lineString = LineString.fromPolyline(mRoute.getGeometry(), Constants.OSRM_PRECISION_V5);
        List<Position> coordinates = lineString.getCoordinates();
        final LatLng[] points = new LatLng[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {

            // round latitude and longitude
            Double newLat = coordinates.get(i).getLatitude();
            Double newLong = coordinates.get(i).getLongitude();
            // move decimal one place to the left
            newLat /= 10;
            newLong /= 10;

            points[i] = new LatLng(newLat, newLong);
        }

        // draw points on the MapView
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.addPolyline(new PolylineOptions()
                        .add(points)
                        .color(Color.parseColor("#009688"))
                        .width(5));
            }
        });
    }

    private void sendNotification(int howManyConversations, int messagesPerConversation) {
        if (mBound) {
            Message msg = Message.obtain(null, MessagingService.MSG_SEND_NOTIFICATION,
                    howManyConversations, messagesPerConversation);
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                Log.e(TAG, "Error sending a message", e);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        CURRENT_LATITUDE = location.getLatitude();
        CURRENT_LONGITUDE = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
        getActivity().bindService(new Intent(getActivity(), MessagingService.class), mConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();

        // create mReceiver
        mReceiver = new MapFilterReceiver();

        // listen for update intent
        IntentFilter update = new IntentFilter(ACTION_UPDATE_MAP);

        // register the mReceiver
        getActivity().registerReceiver(mReceiver, update);
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();

        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
        if (mBound) {
            getActivity().unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();

        // End the navigation session
        mNavigation.endNavigation();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    class MapFilterReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // update the map
            updateMap();
        }
    }
}
