package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.mapbox.mapboxsdk.maps.MapView;

import java.util.ArrayList;

import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.ACTION_UPDATE_LIST;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.ACTION_UPDATE_MAP;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.LIST_FILTERED;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.LIST_FRAGMENT_ID;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.MAP_ACTIVE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.MAP_FRAGMENT_ID;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.MAX_DISTANCE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.MAX_PRICE;

public class MapActivity extends AppCompatActivity implements View.OnClickListener, ParkingSpotsInterface {

    private static final String TAG = "MapActivity";

    // get access to toolbar when getting directions
    Toolbar mToolbar;
    // get access to TextView for searching
    private TextView mSearch;
    // get access to ImageView for filtering
    private ImageView mFilter;

    // array to hold all parking spaces
    ArrayList<ParkingLoc> mAllParkingSpaces = new ArrayList<>();
    // create arrayList to hold filtered data
    ArrayList<ParkingLoc> mFilteredParkingSpaces = new ArrayList<>();
    FloatingActionButton mListFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mapFrameLayout, MapFragment.newInstance()).commit();
            ParkingFinderUtils.MAP_ACTIVE = true;
        }

        // inflate navigation fragments
        // they are hidden until the user starts getting directions
        getSupportFragmentManager().beginTransaction().replace(R.id.bottomNav, BottomNavFragment.newInstance()).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.topNav, TopNavFragment.newInstance()).commit();

        // get access to the SearchView & Toolbar
        mSearch = (TextView) findViewById(R.id.searchBar);
        mSearch.setOnClickListener(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mFilter = (ImageView) findViewById(R.id.filter);
        mFilter.setOnClickListener(this);

        // update toolbar UI
        // update UI
        mToolbar.setTitle("Directions");

        // set up back button on Toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // set up navigation
        // so user can go back
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check which fragment is active
                if (MAP_ACTIVE) {
                    directionsStopped();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.mapFrameLayout, MapFragment.newInstance()).commit();
                }
            }
        });

        FloatingActionButton accountFab = (FloatingActionButton) findViewById(R.id.accountFab);
        accountFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // go to History Activity
                Intent toHistory = new Intent(getApplicationContext(), HistoryActivity.class);
                startActivity(toHistory);
            }
        });

        mListFab = (FloatingActionButton) findViewById(R.id.listFab);
        mListFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check which fragment is active
                if (MAP_ACTIVE) {
                    // go to ListView Activity
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.mapFrameLayout, ListFragment.newInstance()).commit();
                    MAP_ACTIVE = false;
                    // update mListFab icon
                    mListFab.setImageResource(R.drawable.map_icon);

                } else {
                    // go to MapView Activity
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.mapFrameLayout, MapFragment.newInstance()).commit();
                    MAP_ACTIVE = true;
                    // update mListFab icon
                    mListFab.setImageResource(R.drawable.list_icon);
                }
            }
        });
    }

    @Override
    public void startTask(MapView mapView, Double currentLat, Double currentLong, LocationBaseAdapter baseAdapter, ParkingSpotsInterface spotsInterface) {
        // first empty the arrayList
        mAllParkingSpaces.clear();
        MapTask task = new MapTask(this, mAllParkingSpaces, mapView, currentLat, currentLong, baseAdapter, spotsInterface);
        task.execute();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.searchBar) {
            openAutocompleteActivity();
        } else if (v.getId() == R.id.filter) {
            // display alert dialog
            showFilterDialog();
        }
    }

    private void openAutocompleteActivity() {
        try {
            // check if Google Play Services is enabled
            Intent startSearch = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(startSearch, 1);
        } catch (GooglePlayServicesRepairableException e) {
            // if Google Play Services is either not installed or not up to date...
            // tell user the issue
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 ).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // if Google Play Services is not available...
            // tell user the issue
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check to see whether user is searching from Map or List
        if (MAP_ACTIVE) {
            // handle onActivityResult in MapFragment
            MapFragment mapFragment = (MapFragment) getSupportFragmentManager().findFragmentById(MAP_FRAGMENT_ID);
            mapFragment.onActivityResult(requestCode, resultCode, data);
        } else {
            // handle onActivityResult in ListFragment
            ListFragment listFragment = (ListFragment) getSupportFragmentManager().findFragmentById(LIST_FRAGMENT_ID);
            listFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void directionsStarted() {
        // hide linear layout that contains Search
        LinearLayout layout = (LinearLayout) findViewById(R.id.actionBarLayout);
        layout.setVisibility(View.GONE);
        // hide filter
        ImageButton filter = (ImageButton) findViewById(R.id.filter);
        filter.setVisibility(View.GONE);
        // hide accountFab & listFab
        FloatingActionButton accountFab = (FloatingActionButton) findViewById(R.id.accountFab);
        accountFab.setVisibility(View.GONE);
        FloatingActionButton listFab = (FloatingActionButton) findViewById(R.id.listFab);
        listFab.setVisibility(View.GONE);

        // show toolbar
        mToolbar.setVisibility(View.VISIBLE);
        // show navigation
        FrameLayout topNav = (FrameLayout) findViewById(R.id.topNav);
        FrameLayout bottomNav = (FrameLayout) findViewById(R.id.bottomNav);
        topNav.setVisibility(View.VISIBLE);
        bottomNav.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateDirectionsUI(String distance, String time, String waypoint,
                                   String distanceTemp, String currentStreet,
                                   String instruction, String arrivalTime) {
        // update bottomNav UI
        TextView timeText = (TextView)findViewById(R.id.timeText);
        timeText.setText(time);

        TextView distanceText = (TextView)findViewById(R.id.distanceText);
        distanceText.setText(distance);

        TextView destinationText = (TextView)findViewById(R.id.destinationText);
        destinationText.setText(waypoint);

        // update topNav UI
        TextView distanceNextTurnText = (TextView)findViewById(R.id.distanceNextTurnText);
        distanceNextTurnText.setText(distanceTemp);

        TextView currentStreetText = (TextView)findViewById(R.id.currentStreetText);
        currentStreetText.setText(currentStreet);

        TextView instructionText = (TextView)findViewById(R.id.instructionText);
        instructionText.setText(instruction);

        TextView arrivalText = (TextView)findViewById(R.id.arrivalText);
        arrivalTime = "Arrival Time: " + arrivalTime;
        arrivalText.setText(arrivalTime);
    }

    @Override
    public void directionsStopped() {
        // show linear layout that contains Search
        LinearLayout layout = (LinearLayout) findViewById(R.id.actionBarLayout);
        layout.setVisibility(View.VISIBLE);
        // show filter
        ImageButton filter = (ImageButton) findViewById(R.id.filter);
        filter.setVisibility(View.VISIBLE);
        // show accountFab & listFab
        FloatingActionButton accountFab = (FloatingActionButton) findViewById(R.id.accountFab);
        accountFab.setVisibility(View.VISIBLE);
        FloatingActionButton listFab = (FloatingActionButton) findViewById(R.id.listFab);
        listFab.setVisibility(View.VISIBLE);

        // hide toolbar
        mToolbar.setVisibility(View.GONE);

        // hide navigation
        FrameLayout topNav = (FrameLayout) findViewById(R.id.topNav);
        FrameLayout bottomNav = (FrameLayout) findViewById(R.id.bottomNav);
        topNav.setVisibility(View.GONE);
        bottomNav.setVisibility(View.GONE);
    }

    @Override
    public ArrayList<ParkingLoc> getAllParkingSpaces() {
        if (LIST_FILTERED) {
            return mFilteredParkingSpaces;
        }
        return mAllParkingSpaces;
    }

    private void showFilterDialog() {
        // create an AlertDialog
        // so the user can filter map
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Filters");
        // so the user can click away from the AlertDialog to cancel
        builder.setCancelable(true);

        // inflate view to be displayed in alert dialog
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.filter_dialog_layout, null);
        // get access to priceFormatted user has input
        final EditText maxPrice = (EditText)dialogView.findViewById(R.id.filterPriceText);
        final EditText maxDistance = (EditText)dialogView.findViewById(R.id.filterDistanceText);

        // display view in dialog
        builder.setView(dialogView);

        builder.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // first make sure the fields are not empty
                // if fields are filled out correctly, update map
                if (checkDistance(maxDistance)) {
                    Integer max = Integer.parseInt(maxDistance.getText().toString().trim());
                    filterDistance(max);
                }

                if (checkPrice(maxPrice)) {
                    Integer max = Integer.parseInt(maxPrice.getText().toString().trim());
                    filterPrice(max);
                }

                // send broadcast to update Map and List
                doneFiltering();

                // save the filters
                saveFilters(maxDistance.getText().toString(), maxPrice.getText().toString());

                // if not, do nothing
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user cancelled
                // do nothing
            }
        });


        // build and show the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void saveFilters(String maxDistance, String maxPrice) {
        // get access to database
        DatabaseHelper helper = DatabaseHelper.getInstance(this);
        helper.newFilter(maxDistance, maxPrice);
        Log.i(TAG, "saveFilters: SAVING FILTERS");
    }

    private boolean checkPrice(EditText maxPrice) {
        return maxPrice.getText().toString().trim().length() > 0;
    }

    private boolean checkDistance(EditText maxDistance) {
        return maxDistance.getText().toString().trim().length() > 0;
    }

    private void filterPrice(Integer maxPrice) {
        mFilteredParkingSpaces.clear();

        // add everything from mAllParkingSpaces to filteredParkingSpaces
        for (int i = 0; i < mAllParkingSpaces.size(); i++) {
            mFilteredParkingSpaces.add(mAllParkingSpaces.get(i));
        }

        // now filter by price
        for (int i = mFilteredParkingSpaces.size()-1; i >= 0; i--) {
            // check if each item has a price greater than maxPrice
            if (mFilteredParkingSpaces.get(i).price > maxPrice) {
                // if it does, remove it from the list
                mFilteredParkingSpaces.remove(i);
            }
        }
    }

    private void filterDistance(Integer maxDistance) {
        mFilteredParkingSpaces.clear();

        // add everything from mAllParkingSpaces to filteredParkingSpaces
        for (int i = 0; i < mAllParkingSpaces.size(); i++) {
            mFilteredParkingSpaces.add(mAllParkingSpaces.get(i));
        }

        // now filter by price
        for (int i = mFilteredParkingSpaces.size()-1; i >= 0; i--) {
            // check if each item has a price greater than maxPrice
            if (mFilteredParkingSpaces.get(i).getDistance() > maxDistance) {
                // if it does, remove it from the list
                mFilteredParkingSpaces.remove(i);
            }
        }
    }

    private void doneFiltering() {
        LIST_FILTERED = true;
        // update Map & List
        Intent updateMap = new Intent(ACTION_UPDATE_MAP);
        Intent updateList = new Intent(ACTION_UPDATE_LIST);
        sendBroadcast(updateMap);
        sendBroadcast(updateList);
    }

    @Override
    public void getSavedFilters() {
        DatabaseHelper helper = DatabaseHelper.getInstance(this);
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, null, null, null, null, null, null);

        // check if the cursor is not null & has values
        if (cursor != null && cursor.getCount() > 0) {
            // add each item from cursor to the array

            // first loop through cursor
            while (cursor.moveToNext()) {
                // add each new item to the array
                String maxDistance = cursor.getString(cursor.getColumnIndex(DatabaseHelper.MAX_DISTANCE));
                String maxPrice = cursor.getString(cursor.getColumnIndex(DatabaseHelper.MAX_PRICE));

                // filter by the saved data
                if (maxPrice.length() > 0) {
                    MAX_PRICE = maxPrice;
                    filterPrice(Integer.parseInt(maxPrice));
                }

                if (maxDistance.length() > 0) {
                    MAX_DISTANCE = maxDistance;
                    filterDistance(Integer.parseInt(maxDistance));
                }
            }

            // close when finished
            cursor.close();

            // send broadcast to update Map and List
            doneFiltering();
        }
        // cursor is null/database is empty
        // do nothing
    }
}
