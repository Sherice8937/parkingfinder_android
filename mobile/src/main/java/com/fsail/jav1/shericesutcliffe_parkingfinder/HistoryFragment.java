package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ObjectOutputStream;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by Sherice on 9/2/17.
 */

public class HistoryFragment extends Fragment {

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ArrayList<Reservation> mAllPastRes = new ArrayList<>();
    private ArrayList<Reservation> mAllFutureRes = new ArrayList<>();
    private ArrayList<Reservation> mAllReservations = new ArrayList<>();

    HistoryBaseAdapter mFutureAdapter;
    HistoryBaseAdapter mPastAdapter;

    public static HistoryFragment newInstance() {

        Bundle args = new Bundle();

        HistoryFragment fragment = new HistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.history_fragment, container, false);

        loadReservations();

        ListView pastList = (ListView)inflateView.findViewById(R.id.pastResListView);
        ListView futureList = (ListView)inflateView.findViewById(R.id.futureResListView);

        // set up adapters for lists
        mFutureAdapter = new HistoryBaseAdapter(getContext(), mAllFutureRes);
        mPastAdapter = new HistoryBaseAdapter(getContext(), mAllPastRes);
        pastList.setAdapter(mPastAdapter);
        futureList.setAdapter(mFutureAdapter);

        // update lists
        mFutureAdapter.notifyDataSetChanged();
        mPastAdapter.notifyDataSetChanged();

        return inflateView;
    }

    // download data from Firebase
    private void loadReservations() {
        if (mAuth.getCurrentUser() != null) {
            DatabaseReference ref = mDatabase.child("users").child(mAuth.getCurrentUser().getUid());
            ref.addValueEventListener(historyListener);
        }
    }

    private final ValueEventListener historyListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // Get map of reservations in dataSnapshot
            collectReservations((Map<String,Object>) dataSnapshot.getValue());
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Getting reservation failed, log a message
            Toast.makeText(getContext(), "An error occurred, please try again later.",
                    Toast.LENGTH_SHORT).show();
        }
    };

    private void collectReservations(Map<String,Object> reservations) {
        // clear the list
        mAllReservations.clear();

        // iterate through each reservation, ignoring their UID
        if (reservations != null) {
            for (Map.Entry<String, Object> entry : reservations.entrySet()) {

                // get user map
                Map singleRes = (Map) entry.getValue();

                if (singleRes == null || singleRes.size() <= 0) {
                    break;
                }

                // get phone field and append to list
                mAllReservations.add(new Reservation(
                        singleRes.get("title").toString(),
                        singleRes.get("price").toString(),
                        singleRes.get("time").toString(),
                        singleRes.get("date").toString()));

                // separate the lists based on date
                Format formatter = new SimpleDateFormat("MM-dd-yyyy");
                String todayString = formatter.format(new Date());

                SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");

                // compare each date in the array to the date today
                try {
                    Date today = sdf.parse(todayString);
                    for (int i = 0; i < mAllReservations.size(); i++) {
                        Date date2 = sdf.parse(mAllReservations.get(i).getDate());
                        if (today.compareTo(date2) <= 0) {
                            // add to Future Reservations
                            mAllFutureRes.add(mAllReservations.get(i));
                        } else {
                            // add to Past Reservations
                            mAllPastRes.add(mAllReservations.get(i));
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "An error occurred loading history. Please try again later.",
                            Toast.LENGTH_LONG).show();
                }

                // refresh the list, displaying new data
                mFutureAdapter.notifyDataSetChanged();
                mPastAdapter.notifyDataSetChanged();
            }
        }
    }
}
