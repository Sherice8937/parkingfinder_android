package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.service.carrier.CarrierMessagingService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wallet.Cart;
import com.google.android.gms.wallet.FullWallet;
import com.google.android.gms.wallet.FullWalletRequest;
import com.google.android.gms.wallet.LineItem;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.NotifyTransactionStatusRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentMethodTokenizationType;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.gms.wallet.fragment.BuyButtonText;
import com.google.android.gms.wallet.fragment.Dimension;
import com.google.android.gms.wallet.fragment.SupportWalletFragment;
import com.google.android.gms.wallet.fragment.WalletFragmentInitParams;
import com.google.android.gms.wallet.fragment.WalletFragmentMode;
import com.google.android.gms.wallet.fragment.WalletFragmentOptions;
import com.google.android.gms.wallet.fragment.WalletFragmentStyle;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_ADDRESS;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_PRICE;
import static com.fsail.jav1.shericesutcliffe_parkingfinder.ParkingFinderUtils.EXTRA_URL;

/**
 * Created by Sherice on 8/21/17.
 * Parking Finder
 */

@SuppressWarnings("ALL")
public class MapDetailsFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    public static final String WALLET_FRAGMENT_ID = "wallet_fragment";
    public static final int MASKED_WALLET_REQUEST_CODE = 888;
    public static final int FULL_WALLET_REQUEST_CODE = 889;

    private SupportWalletFragment mWalletFragment;
    private GoogleApiClient mGoogleApiClient;
    private MaskedWallet mMaskedWallet;
    private FullWallet mFullWallet;

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private Button mReserve;
    FrameLayout mWalletHolder;

    double mTotalCost;


    public static MapDetailsFragment newInstance() {

        Bundle args = new Bundle();

        MapDetailsFragment fragment = new MapDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ParkingFinderUtils.MAP_DETAILS_FRAGMENT_ID = MapDetailsFragment.super.getId();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        // build the Google API Client
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wallet.API, new Wallet.WalletOptions.Builder()
                        .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                        .setTheme(WalletConstants.THEME_LIGHT)
                        .build())
                .build();

        // create Masked Wallet for payment
        createWallet();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.map_details_fragment, container, false);

        TextView price = (TextView)inflateView.findViewById(R.id.details_priceText);
        TextView address = (TextView)inflateView.findViewById(R.id.details_addressText);
        TextView spots = (TextView)inflateView.findViewById(R.id.details_spotsText);
        TextView description = (TextView)inflateView.findViewById(R.id.details_descriptionText);
        TextView directions = (TextView)inflateView.findViewById(R.id.details_directionsText);
        mWalletHolder = (FrameLayout)inflateView.findViewById(R.id.wallet_button_holder);
        mReserve = (Button)inflateView.findViewById(R.id.reserveButton);
        mReserve.setOnClickListener(this);

        // get access to the data passed in
        Intent mapData = getActivity().getIntent();
        // update UI
        price.setText(mapData.getStringExtra(EXTRA_PRICE));
        address.setText(mapData.getStringExtra(EXTRA_ADDRESS));
        String apiUrl = mapData.getStringExtra(EXTRA_URL);

        // start task to download new data from apiUrl
        MapDetailsTask task = new MapDetailsTask(getActivity(), apiUrl, spots, description, directions);
        task.execute();

        return inflateView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.reserveButton) {
            // show alert dialog
            showReserveDialog();
        }
    }

    private void showReserveDialog() {
        // create an AlertDialog
        // so the user can filter map
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Reservation Details");
        // so the user can click away from the AlertDialog to cancel
        builder.setCancelable(true);

        // inflate view to be displayed in alert dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.duration_dialog_layout, null);
        // get access to priceFormatted user has input
        final EditText fromTime = (EditText)dialogView.findViewById(R.id.fromTimeText);
        final EditText toTime = (EditText)dialogView.findViewById(R.id.toTimeText);
        final TextView totalText = (TextView)dialogView.findViewById(R.id.totalText);
        // display view in dialog
        builder.setView(dialogView);

        builder.setPositiveButton("Reserve", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // first make sure the fields are not empty
                // if fields are filled out correctly...
                if (checkFields(fromTime, toTime)) {
                    // create a Reservation object, & save to the database
                    if (mTotalCost > 0) {
                        saveReservation(fromTime.getText().toString(), toTime.getText().toString(), mTotalCost);
                    } else {
                        // display error
                        Toast.makeText(getContext(), "Invalid duration chosen. Please try again",
                                Toast.LENGTH_SHORT).show();
                    }
                }

                // if not, display error
                Toast.makeText(getContext(), "Fields cannot be blank or empty", Toast.LENGTH_SHORT).show();

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user cancelled
                // do nothing
            }
        });


        // build and show the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();

        // add TextChangedListeners to calculate the total
        fromTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (fromTime.getText().toString().trim().length() > 0 &&
                        toTime.getText().toString().trim().length() > 0) {
                    getTotalCost(fromTime.getText().toString(), toTime.getText().toString(), totalText);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // do nothing
            }
        });

        toTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (fromTime.getText().toString().trim().length() > 0 &&
                        toTime.getText().toString().trim().length() > 0) {
                    getTotalCost(fromTime.getText().toString(), toTime.getText().toString(), totalText);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // do nothing
            }
        });
    }

    private void getTotalCost(String fromTime, String toTime, TextView total) {
        // get rid of the colon, so we can multiply the dates
        fromTime = fromTime.replace(":", "");
        toTime = toTime.replace(":", "");

        // convert to doubles's
        Double fromDouble = Double.parseDouble(fromTime);
        Double toDouble = Double.parseDouble(toTime);

        // divide
        double difference = toDouble / fromDouble;

        // multiply the price by this difference
        Intent dataIntent = getActivity().getIntent();
        String price = dataIntent.getStringExtra(EXTRA_PRICE);
        price = price.replace("$", "");

        double priceDouble = Double.parseDouble(price);
        priceDouble = priceDouble * difference;
        // formatter to round double to second decimal place
        DecimalFormat formatter = new DecimalFormat("###.##");

        // update total
        mTotalCost = Double.parseDouble(formatter.format(priceDouble));

        // update TextView to display new total
        total.setText("Total: $" + mTotalCost);
    }

    private boolean checkFields(EditText fromTime, EditText toTime) {
        if (fromTime.getText().toString().trim().length() > 0) {
            if (toTime.getText().toString().trim().length() > 0) {
                return true;
            } else {
                toTime.setError("Field cannot be blank");
            }
        } else {
            fromTime.setError("Field cannot be blank");
            return false;
        }

        return false;
    }

    private void saveReservation(String fromTime, String toTime, double totalCost) {
        if (mAuth.getCurrentUser() != null) {
            // get the current date
            Format formatter = new SimpleDateFormat("MM-dd-yyyy");
            String date = formatter.format(new Date());

            // get the title
            Intent dataIntent = getActivity().getIntent();
            String title = dataIntent.getStringExtra(ParkingFinderUtils.EXTRA_TITLE);

            // lastly, add the new Reservation to the user's history
            mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).push().setValue(new Reservation
                    (title, String.valueOf(totalCost), fromTime + " - " + toTime, date));

            Toast.makeText(getContext(), "Reservation saved!", Toast.LENGTH_SHORT).show();
        }
    }

    private void createWallet() {
        Wallet.Payments.isReadyToPay(mGoogleApiClient).setResultCallback(
                new ResultCallback<BooleanResult>() {
                    @Override
                    public void onResult(@NonNull BooleanResult booleanResult) {
                        if (booleanResult.getStatus().isSuccess()) {
                            if (booleanResult.getValue()) {
                                setUpWalletFragment();
                            } else {
                                // display regular reserve action
                                mReserve.setVisibility(View.VISIBLE);
                                // hide Android Pay frame layout
                                mWalletHolder.setVisibility(View.GONE);
                            }
                        } else {
                            // Error making isReadyToPay call
                        }
                    }
                });
    }

    private void setUpWalletFragment() {
        // Check if WalletFragment exists
        mWalletFragment = (SupportWalletFragment) getActivity().getSupportFragmentManager()
                .findFragmentByTag(WALLET_FRAGMENT_ID);

        // It does not, add it
        if(mWalletFragment == null) {
            // Wallet fragment style
            WalletFragmentStyle walletFragmentStyle = new WalletFragmentStyle()
                    .setBuyButtonText(BuyButtonText.BUY_NOW)
                    .setBuyButtonWidth(Dimension.MATCH_PARENT);

            // Wallet fragment options
            WalletFragmentOptions walletFragmentOptions = WalletFragmentOptions.newBuilder()
                    .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                    .setFragmentStyle(walletFragmentStyle)
                    .setTheme(WalletConstants.THEME_LIGHT)
                    .setMode(WalletFragmentMode.BUY_BUTTON)
                    .build();


            // Instantiate the WalletFragment
            mWalletFragment = SupportWalletFragment.newInstance(walletFragmentOptions);

            // Initialize the WalletFragment
            WalletFragmentInitParams.Builder startParamsBuilder =
                    WalletFragmentInitParams.newBuilder()
                            .setMaskedWalletRequest(generateMaskedWalletRequest())
                            .setMaskedWalletRequestCode(MASKED_WALLET_REQUEST_CODE)
                            .setAccountName("Parking Finder");
            mWalletFragment.initialize(startParamsBuilder.build());

            // add the fragment to the UI
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.wallet_button_holder, mWalletFragment, WALLET_FRAGMENT_ID)
                    .commit();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: ON ACTIVITY RESULT HIT...................");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case MASKED_WALLET_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        mMaskedWallet = data.getParcelableExtra
                                (WalletConstants.EXTRA_MASKED_WALLET);
                        requestFullWallet();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user canceled the operation
                        break;
                    case WalletConstants.RESULT_ERROR:
                        handleError(resultCode);
                        break;
                    default:
                        handleError(resultCode);
                        break;
                }
                break;
            case FULL_WALLET_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        mFullWallet = data.getParcelableExtra(WalletConstants.EXTRA_FULL_WALLET);
                        // Show the credit card number
                        if (mFullWallet != null) {
                            // Update transaction status
                            Wallet.Payments.notifyTransactionStatus(mGoogleApiClient,
                                    generateNotifyTransactionStatusRequest(
                                            mFullWallet.getGoogleTransactionId(),
                                            NotifyTransactionStatusRequest.Status.SUCCESS));
                            // display reserve dialog
                            showReserveDialog();
                        }
                        break;
                    case WalletConstants.RESULT_ERROR:
                        Toast.makeText(getContext(), "An Error Occurred", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
        }
    }

    public void requestFullWallet() {
        if (mMaskedWallet == null) {
            Toast.makeText(getContext(), "No masked wallet, can't confirm", Toast.LENGTH_SHORT).show();
            return;
        }
        Wallet.Payments.loadFullWallet(mGoogleApiClient,
                generateFullWalletRequest(mMaskedWallet.getGoogleTransactionId()),
                FULL_WALLET_REQUEST_CODE);
    }

    private MaskedWalletRequest generateMaskedWalletRequest() {
        PaymentMethodTokenizationParameters parameters =
                PaymentMethodTokenizationParameters.newBuilder()
                        .setPaymentMethodTokenizationType(PaymentMethodTokenizationType.NETWORK_TOKEN)
                        .addParameter("publicKey", "BILLj5oIxF5eO3l0gWr9jZ4PTYePJRx4Sp/PhuomJKuugGknWFTo8MDWN3dpmifBje9cNXo7TnMpdE4wONa1yAs=")
                        .build();

        MaskedWalletRequest maskedWalletRequest =
                MaskedWalletRequest.newBuilder()
                        .setPaymentMethodTokenizationParameters(parameters)
                        .setMerchantName("Parking Finder")
                        .setPhoneNumberRequired(true)
                        .setShippingAddressRequired(true)
                        .setCurrencyCode("USD")
                        .setCart(Cart.newBuilder()
                                .setCurrencyCode("USD")
                                .setTotalPrice("10.00")
                                .addLineItem(LineItem.newBuilder()
                                        .setCurrencyCode("USD")
                                        .setDescription("Google I/O Sticker")
                                        .setQuantity("1")
                                        .setUnitPrice("10.00")
                                        .setTotalPrice("10.00")
                                        .build())
                                .build())
                        .setEstimatedTotalPrice("15.00")
                        .build();
        return maskedWalletRequest;
    }

    private FullWalletRequest generateFullWalletRequest(String googleTransactionId) {
        FullWalletRequest fullWalletRequest = FullWalletRequest.newBuilder()
                .setGoogleTransactionId(googleTransactionId)
                .setCart(Cart.newBuilder()
                        .setCurrencyCode("USD")
                        .setTotalPrice("10.10")
                        .addLineItem(LineItem.newBuilder()
                                .setCurrencyCode("USD")
                                .setDescription("Google I/O Sticker")
                                .setQuantity("1")
                                .setUnitPrice("10.00")
                                .setTotalPrice("10.00")
                                .build())
                        .addLineItem(LineItem.newBuilder()
                                .setCurrencyCode("USD")
                                .setDescription("Tax")
                                .setRole(LineItem.Role.TAX)
                                .setTotalPrice(".10")
                                .build())
                        .build())
                .build();
        return fullWalletRequest;
    }

    public static NotifyTransactionStatusRequest generateNotifyTransactionStatusRequest(
            String googleTransactionId, int status) {
        return NotifyTransactionStatusRequest.newBuilder()
                .setGoogleTransactionId(googleTransactionId)
                .setStatus(status)
                .build();
    }

    protected void handleError(int errorCode) {
        Log.i(TAG, "handleError: HANDLER ERROR HIT............");
        switch (errorCode) {
            case WalletConstants.ERROR_CODE_SPENDING_LIMIT_EXCEEDED:
                Toast.makeText(getContext(), "Spending limit exceeded.",
                        Toast.LENGTH_LONG).show();
                break;
            case WalletConstants.ERROR_CODE_INVALID_PARAMETERS:
                Toast.makeText(getContext(), "Invalid parameters. Please try again",
                        Toast.LENGTH_LONG).show();
            case WalletConstants.ERROR_CODE_AUTHENTICATION_FAILURE:
                Toast.makeText(getContext(), "Failure authenticating...",
                        Toast.LENGTH_LONG).show();
            case WalletConstants.ERROR_CODE_BUYER_ACCOUNT_ERROR:
                Toast.makeText(getContext(), "An error occurred. Please check your Android Pay account and try again.",
                        Toast.LENGTH_LONG).show();
            case WalletConstants.ERROR_CODE_MERCHANT_ACCOUNT_ERROR:
                Toast.makeText(getContext(), "An error has occurred on our end. Please come back and try again later.",
                        Toast.LENGTH_LONG).show();
            case WalletConstants.ERROR_CODE_SERVICE_UNAVAILABLE:
                Toast.makeText(getContext(), "Pay services unavailable. Please try again",
                        Toast.LENGTH_LONG).show();
            case WalletConstants.ERROR_CODE_UNSUPPORTED_API_VERSION:
                Toast.makeText(getContext(), "API version unsupported. Please update to the latest version.",
                        Toast.LENGTH_LONG).show();
            case WalletConstants.ERROR_CODE_UNKNOWN:
                Toast.makeText(getContext(), "Unknown error: " + errorCode,
                        Toast.LENGTH_LONG).show();
            default:
                // unrecoverable error
                Toast.makeText(getContext(), "Google Wallet is unavailable. Please try again.",
                        Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        // GoogleApiClient is connected, we don't need to do anything here
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // GoogleApiClient is temporarily disconnected, no action needed
        Toast.makeText(getContext(), "Connection Suspended.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        // GoogleApiClient failed to connect, we should log the error and retry
        Toast.makeText(getContext(), "There was an error, please try again.", Toast.LENGTH_SHORT).show();
    }
}
