// Sherice Sutcliffe
// MDF III - 1706
// DatabaseHelper.java

package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_FILE = "filtersDatabase.db";
    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper INSTANCE = null;
    private final SQLiteDatabase database;

    // set up table name and column names
    static final String TABLE_NAME = "filters";
    private static final String ID = "_id";
    static final String MAX_DISTANCE = "maxDistance";
    static final String MAX_PRICE = "maxPrice";


    private DatabaseHelper(Context context) {
        super(context, DATABASE_FILE, null, DATABASE_VERSION);
        database = getWritableDatabase();
    }

    public static DatabaseHelper getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseHelper(context);
        }
        return INSTANCE;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
                TABLE_NAME + " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                MAX_DISTANCE + " TEXT, " +
                MAX_PRICE + " TEXT)";
        db.execSQL(CREATE_TABLE);
    }

    public void newFilter(String maxDistance, String maxPrice) {
        // first delete whatever filters are already saved
        deleteFilter();

        ContentValues contentValues = new ContentValues();
        contentValues.put(MAX_DISTANCE, maxDistance);
        contentValues.put(MAX_PRICE, maxPrice);

        database.insert(TABLE_NAME, null, contentValues);
    }

    public void deleteFilter() {
        String whereClause =
                MAX_PRICE + "=? AND " +
                        MAX_DISTANCE + "=?";

        String[] whereArgs = new String[] {
                MAX_PRICE,
                MAX_DISTANCE
        };

        // delete where parameters match
        // execute SQL statement
        database.delete(TABLE_NAME, whereClause, whereArgs);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
