package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.os.Parcelable;

/**
 * Created by Sherice on 8/4/17.
 * Parking Finder
 */

class ParkingLoc {

    private final String locationName;
    private final String address;
    private final String city;
    private final String state;
    private final String zip;
    private final int distance;
    private final String priceFormatted;
    private final String apiUrl;
    final Integer price;
    private Double latitude;
    private Double longitude;


    public ParkingLoc(String locationName, String address, String city, String state, String zip,
                      int distance, String priceFormatted, Integer price, String apiUrl,
                      Double latitude, Double longitude) {
        this.locationName = locationName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.distance = distance;
        this.priceFormatted = priceFormatted;
        this.price = price;
        this.apiUrl = apiUrl;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public int getDistance() {
        return distance / 5280;
    }

    public String getPriceFormatted() {
        return priceFormatted;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
