package com.fsail.jav1.shericesutcliffe_parkingfinder;

import com.mapbox.mapboxsdk.maps.MapView;

import java.util.ArrayList;

/**
 * Created by Sherice on 9/3/17.
 * Parking Finder
 */

public interface ParkingSpotsInterface {

    ArrayList<ParkingLoc> getAllParkingSpaces();
    void directionsStarted();
    void directionsStopped();

    void startTask(MapView mapView, Double currentLat, Double currentLong,
                   LocationBaseAdapter baseAdapter, ParkingSpotsInterface spotsInterface);

    void updateDirectionsUI(String distance, String time, String waypoint, String distanceTemp,
                            String currentStreet, String instruction, String arrivalTime);
    void getSavedFilters();
}
