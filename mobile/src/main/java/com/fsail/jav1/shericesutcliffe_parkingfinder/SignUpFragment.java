package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sherice on 8/19/17.
 * Parking Finder
 */

public class SignUpFragment extends Fragment implements View.OnClickListener {

    private EditText mEmail;
    private EditText mPassword;
    private EditText mRePassword;
    private FirebaseAuth mAuth;
    private CallbackManager callbackManager;

    public static SignUpFragment newInstance() {

        Bundle args = new Bundle();

        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.sign_up_fragment, container, false);

        mEmail = (EditText) inflateView.findViewById(R.id.emailText);
        mPassword = (EditText) inflateView.findViewById(R.id.passwordText);
        mRePassword = (EditText) inflateView.findViewById(R.id.passwordText_01);

        TextView signIn = (TextView)inflateView.findViewById(R.id.signInText);
        LoginButton fbButton = (LoginButton) inflateView.findViewById(R.id.fb_login_button);
        Button signUp = (Button)inflateView.findViewById(R.id.signUpButton);

        fbButton.setOnClickListener(this);
        signIn.setOnClickListener(this);
        signUp.setOnClickListener(this);

        fbButton.setFragment(this);
        fbButton.setReadPermissions("email", "public_profile");
        // Callback registration
        fbButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }
            @Override
            public void onCancel() {
                // Action cancelled
                // do nothing
            }
            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(getContext(), "There was an error, please try again.",
                        Toast.LENGTH_SHORT).show();
                // do nothing
            }
        });

        return inflateView;
    }

    @Override
    public void onClick(View v) {
        // find out which view was clicked
        if (v.getId() == R.id.signInText) {
            // display the Sign In Fragment
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.signInFrameLayout, SignInFragment.newInstance()).commit();

        } else if (v.getId() == R.id.signUpButton) {
            // check if email and password are correct
            if (checkEmail() && checkPassword()) {
                // if they are, create the new account
                createAccount();

                // if not, do nothing
            }
        }
    }

    private boolean checkEmail() {
        // create regex to check format
        String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(mEmail.getText().toString().trim());

        if (matcher.matches()) {
            return true;
        } else {
            mEmail.setError("Invalid Email");
            return false;
        }
    }

    private boolean checkPassword() {
        // check if password is empty or all spaces
        if (mPassword.getText().toString().trim().equals("")) {
            mPassword.setError("Invalid Email");
            return false;
        }

        if (mRePassword.getText().toString().trim().equals("")) {
            mRePassword.setError("Invalid Email");
            return false;
        }

        // check if both passwords match
        if (mPassword.getText().toString().trim().equals(mRePassword.getText().toString().trim())) {
            return true;
        } else {
            mPassword.setError("Passwords must match");
            mRePassword.setError("Passwords must match");
            return false;
        }
    }

    private void createAccount() {
        mAuth.createUserWithEmailAndPassword
                (mEmail.getText().toString().trim(), mPassword.getText().toString().trim())
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // if account was successfully created, go to Map
                            Intent toMap = new Intent(getContext(), MapActivity.class);
                            startActivity(toMap);

                        } else {
                            // if account was not created, display error
                            Toast.makeText(getContext(), "Authentication failed. Please try again",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // if sign in succeeds, do nothing
                            // go the Map
                            Intent toMap = new Intent(getContext(), MapActivity.class);
                            startActivity(toMap);

                        } else {
                            // If sign in fails, display message
                            Toast.makeText(getContext(), "An error occurred, please try again.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
