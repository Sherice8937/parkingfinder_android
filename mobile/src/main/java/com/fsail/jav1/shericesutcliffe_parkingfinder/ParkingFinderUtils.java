package com.fsail.jav1.shericesutcliffe_parkingfinder;

/**
 * Created by Sherice on 8/21/17.
 * Parking Finder
 */

class ParkingFinderUtils {

    public static final String URL = "https://api.parkwhiz.com/search/?";
    public static  final String LATITUDE = "lat=";
    public static  final String LONGITUDE = "lng=";
    public static  final String STARTTIME = "start=1501874818&";
    public static  final String ENDTIME = "end=1501885618&";
    public static  final String KEY = "key=fec4fd64d0fc97d65fa170c55e984caaa5e56111";

    public static final String EXTRA_URL = "sas_parkingfinder_extra_url";
    public static final String EXTRA_ADDRESS = "sas_parkingfinder_extra_address";
    public static final String EXTRA_PRICE = "sas_parkingfinder_extra_price";
    public static final String EXTRA_TITLE = "sas_parkingfinder_extra_title";
    public static String EXTRA_CURRENT_LAT = "sas_parkingfinder_extra_current_latitude";
    public static String EXTRA_CURRENT_LONG = "sas_parkingfinder_extra_current_longitude";
    public static String EXTRA_DEST_LAT = "sas_parkingfinder_extra_destination_latitude";
    public static String EXTRA_DEST_LONG = "sas_parkingfinder_extra_destination_longitude";

    public static String ACTION_UPDATE_MAP = "sas_parkingfinder_action_update_map";
    public static String ACTION_UPDATE_LIST = "sas_parkingfinder_action_update_list";

    public static boolean GET_DIRECTIONS;
    public static boolean LIST_FILTERED;

    public static int MAP_FRAGMENT_ID;
    public static int LIST_FRAGMENT_ID;
    public static int MAP_DETAILS_FRAGMENT_ID;
    public static boolean MAP_ACTIVE;
    public static Double CURRENT_LATITUDE;
    public static Double CURRENT_LONGITUDE;

    public static String MAX_DISTANCE;
    public static String MAX_PRICE;

    public static String NOTIFICATION_MESSAGE;
    public static int CURRENT_STEP = -1;
    public static int TOTAL_STEPS;

}
