package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Sherice on 8/19/17.
 * Parking Finder
 */

public class SignInFragment extends Fragment implements View.OnClickListener {

    private EditText mEmail;
    private EditText mPassword;
    private FirebaseAuth mAuth;
    private CallbackManager mCallbackManager;

    public static SignInFragment newInstance() {

        Bundle args = new Bundle();

        SignInFragment fragment = new SignInFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.sign_in_fragment, container, false);

        mEmail = (EditText)inflateView.findViewById(R.id.emailText);
        mPassword = (EditText)inflateView.findViewById(R.id.passwordText);
        TextView forgotPassword = (TextView)inflateView.findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(this);

        Button signInButton = (Button) inflateView.findViewById(R.id.signInButton);
        signInButton.setOnClickListener(this);

        TextView signUpText = (TextView)inflateView.findViewById(R.id.signUpText);
        signUpText.setOnClickListener(this);

        LoginButton fbButton = (LoginButton) inflateView.findViewById(R.id.fb_login_button);
        fbButton.setOnClickListener(this);

        fbButton.setFragment(this);
        fbButton.setReadPermissions("email", "public_profile");
        // Callback registration
        fbButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }
            @Override
            public void onCancel() {
                // Action cancelled
                // do nothing
            }
            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(getContext(), "There was an error, please try again.",
                        Toast.LENGTH_SHORT).show();
                // do nothing
            }
        });

        return inflateView;
    }

    @Override
    public void onClick(View v) {
        // find out which view was clicked
        if (v.getId() == R.id.signUpText) {
            // display the Sign Up Fragment
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.signInFrameLayout, SignUpFragment.newInstance()).commit();
        }

        if (v.getId() == R.id.signInButton) {
            checkFields();
        }

        if (v.getId() == R.id.forgotPassword) {
            // go to password reset fragment
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.signInFrameLayout, PasswordResetFragment.newInstance()).commit();
        }
    }

    private void checkFields() {
        if (mEmail.getText().toString().trim().length() > 0) {
            if (mPassword.getText().toString().trim().length() > 0) {
                signIn();
            } else {
                mPassword.setError("Password cannot be blank");
            }
        } else {
            mEmail.setError("Email cannot be blank");
        }
    }

    private void signIn() {
        mAuth.signInWithEmailAndPassword
                (mEmail.getText().toString().trim(), mPassword.getText().toString().trim())
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // if sign in successful, go to map
                            Intent toMap = new Intent(getContext(), MapActivity.class);
                            startActivity(toMap);

                        } else {
                            // if sign in fails, display message
                            mEmail.setError("Invalid email or password");
                        }
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // if sign in succeeds, go to Map
                            Intent toMap = new Intent(getContext(), MapActivity.class);
                            startActivity(toMap);

                        } else {
                            // If sign in fails, display error
                            Toast.makeText(getContext(), "An error occurred, please try again",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
