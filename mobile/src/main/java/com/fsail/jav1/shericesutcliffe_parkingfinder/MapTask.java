package com.fsail.jav1.shericesutcliffe_parkingfinder;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.DoubleBuffer;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sherice on 8/21/17.
 * Parking Finder
 */

class MapTask extends AsyncTask<Void, Void, Void> {

    private ArrayList<ParkingLoc> mAllParkingSpaces = new ArrayList<>();
    private final Context mContext;
    private MapView mMapView;
    private Double mLongitude;
    private Double mLatitude;
    LocationBaseAdapter mBaseAdapter;
    ParkingSpotsInterface mInterface;

    public MapTask(Context context, ArrayList<ParkingLoc> _allParkingSpaces, MapView mMapView, Double mLatitude,
                   Double mLongitude, LocationBaseAdapter mBaseAdapter, ParkingSpotsInterface mInterface) {
        mAllParkingSpaces = _allParkingSpaces;
        this.mContext = context;
        this.mMapView = mMapView;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
        this.mBaseAdapter = mBaseAdapter;
        this.mInterface = mInterface;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        // connect to the ParkWhiz URL
        try {
            // Create URL
            URL parkWhizURL = new URL(ParkingFinderUtils.URL +
                    ParkingFinderUtils.LATITUDE + mLatitude.toString() + "&" +
                    ParkingFinderUtils.LONGITUDE + mLongitude.toString() + "&" +
                    ParkingFinderUtils.STARTTIME +
                    ParkingFinderUtils.ENDTIME +
                    ParkingFinderUtils.KEY);

            // Create connection
            HttpsURLConnection myConnection =
                    (HttpsURLConnection) parkWhizURL.openConnection();
            // set request property
            myConnection.setRequestProperty("User-Agent",
                    "fec4fd64d0fc97d65fa170c55e984caaa5e56111-sherice-sutcliffe-request");

            // check for valid response
            if (myConnection.getResponseCode() == 200) {
                // Success
                // get a reference to the input stream of the connection
                InputStream response = myConnection.getInputStream();
                String data = IOUtils.toString(response);

                // parse JSON
                parseJSONData(data);

            } else {
                Toast.makeText(mContext, "An error has occurred.", Toast.LENGTH_SHORT).show();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void parseJSONData(String data) {
        try {
            JSONObject outerMostObj = new JSONObject(data);

            JSONArray parkingListings = outerMostObj.getJSONArray("parking_listings");
            // loop through each parking space returned
            for (int i = 0; i < parkingListings.length(); i++) {
                JSONObject parkingSpace = parkingListings.getJSONObject(i);
                String locationName = parkingSpace.getString("location_name");
                String address = parkingSpace.getString("address");
                String city = parkingSpace.getString("city");
                String state = parkingSpace.getString("state");
                String zip = parkingSpace.getString("zip");
                int distance = parkingSpace.getInt("distance");
                String priceFormatted = parkingSpace.getString("price_formatted");
                Integer price = parkingSpace.getInt("price");
                String apiUrl = parkingSpace.getString("api_url");
                Double latitude = parkingSpace.getDouble("lat");
                Double longitude = parkingSpace.getDouble("lng");

                // add to the array
                mAllParkingSpaces.add(new ParkingLoc
                        (locationName, address, city, state, zip, distance, priceFormatted,
                                price, apiUrl, latitude, longitude));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (mMapView != null) {
            // update the map
            updateMap();
        }

        if (mBaseAdapter != null) {
            mBaseAdapter.notifyDataSetChanged();
        }
    }

    private void updateMap() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                for (int i = 0; i < mAllParkingSpaces.size(); i++) {
                    MarkerViewOptions markerViewOptions = new MarkerViewOptions()
                            .position(new LatLng(mAllParkingSpaces.get(i).getLatitude(),
                                    mAllParkingSpaces.get(i).getLongitude()))
                            .title(mAllParkingSpaces.get(i).getLocationName());
                    if (mAllParkingSpaces.get(i).getDistance() <= 1) {
                        markerViewOptions.snippet("<1 mile away" +
                                " | " + mAllParkingSpaces.get(i).getPriceFormatted());
                    } else {
                        markerViewOptions.snippet((mAllParkingSpaces.get(i).getDistance() + " miles away" +
                                " | " + mAllParkingSpaces.get(i).getPriceFormatted()));
                    }

                    mapboxMap.addMarker(markerViewOptions);
                }
            }
        });

        if (mInterface != null) {
            // update the map based on the user's saved data
            mInterface.getSavedFilters();
        }
    }
}
